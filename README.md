# UFP001 - Painel Analítico

Dashboard de solicitações do Fluig. Com este painel é possível ter uma visão global das solicitações em andamento de um determinado workflow assim como gerenciar os responsáveis e status do processo.