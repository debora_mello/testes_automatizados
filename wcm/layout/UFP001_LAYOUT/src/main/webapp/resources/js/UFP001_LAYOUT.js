/**
 * @copyright   2019 upFlow.me
 * @author      Helbert Campos <helbert@upflow.me>
 */

$(document).ready(function () {

    // adiciona o plugin de retornar ao topo da página
    FLUIGC.backToTop({
        labelType: '${i18n.getTranslation("footer.back.to.top")}',
        style: 'btn-primary',
        horizontalDistance: '30px',
        verticalDistance: '55px',
    });

});