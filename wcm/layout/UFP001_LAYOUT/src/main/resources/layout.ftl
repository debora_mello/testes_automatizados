<#import "/wcm.ftl" as wcm/>
<@wcm.header />

<div class="uF wcm-wrapper-content content">

    <@wcm.menu />
	
    <div class="wcm-all-content">   
        <div id="wcm-content" class="">
            
            <!-- INÍCIO: slot de tabela de itens -->
            <div class="editable-slot slotfull layout-1-1" id="divSlotPrincipal">
                 <@wcm.renderSlot id="slotPrincipal" decorator="false" editableSlot="true" /> 
            </div>
            <!-- FINAL: slot de tabela de itens -->
			
            <!-- INÍCIO: linha do footer -->
            <div id="uf_footer" class="uF page-footer">
                <div class="row">
                    <div class="col-12 col-sm">
						<ul class="info">
							<li>${wcmVersionInfo.getVersionName()} ${wcmVersionInfo.getVersionBuild()}</li>
							<li>2020 &copy; upFlow.me Sistemas Ltda</li>
						</ul>
                    </div>
					<div class="col-12 col-sm-auto">
						<ul class="info">
							<li>Rep.: <span id="ufrepname">UFP_000</span></li>
							<li>v.<span id="ufrepversion">0.0.0</span></li>
							<li>
								<a target="_blank" href="http://upflow.me/" class="uf-link" title="${i18n.getTranslation('footer.link.title')}">
									<#include "uflogo.svg" />
								</a>
							</li>
						</ul>
					</div>
                </div>
            </div>
            <!-- FINAL: linha do footer -->
			
        </div>
    </div>
	
</div>