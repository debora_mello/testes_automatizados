/**
 * @desc        Script padrão do widget
 * @copyright   2019 upFlow.me
 * @author      Helbert Campos <helbert@upflow.me>
 */

var UFP001 = SuperWidget.extend({
    
    //método iniciado quando a widget é carregada
	init: function() {
    	$('span#ufrepname').html(this.reposi); $('span#ufrepversion').html(this.versao);   // atualiza footer
		
		let self = this;
		
		moment.locale(WCMAPI.locale); // define a linguagem do moment
		
		// verifica a licença do componente fluig Store
		this.ufapi.serviceCheckSlotId(function (err, data) {
			self.elements.license.data = data.valid;
			window.license = data.valid; //TODO: retirar isso do global para dificultar para usário mal intensionado
			self.elements.license.init();
			self.startup();
		})
    },

	bindings: {
        local: {
            'save-preferences': ['click_savePreferences']
        },
    },
    
	// salva as preferências da widget
    savePreferences: function () {
        var preferences = {}; // cria dinamicamente o objeto
        
       var holder =[];
        var nameHolder =[];
        $('.'+this.reposi+' #bootstrap-duallistbox-selected-list_processos option').each((idx, cmp) => {
        	holder.push(cmp.value);
        	nameHolder.push(cmp.innerHTML.replace(' ('+cmp.value+')',''));
        });
        
        preferences["processos"]=holder;
        preferences["nomeProcessos"]=nameHolder;
        
        WCMSpaceAPI.PageService.UPDATEPREFERENCES({
            async: true,
            success: (data) => {
                FLUIGC.toast({ title: data.message, message: '', type: 'success' });
            },
            fail: (xhr, message, errorData) => {
            	
            	FLUIGC.message.error({
            	    title: 'Erro ao Salvar Preferências',
            	    message: message,
            	    details: errorData,
            	    label: 'Ok'
            	}, function(el, ev) {
            	    
            	});
            	
            }
        }, this.widgetId, preferences);
	},
	
	// dicionário para strings do JavaScript
    i18n: {
    	msg_error: {
    		pt_BR: "Falha ao obter informações do servidor",
    		en_US: "Failed to get process",
    		es: "No se pudo obtener el proceso"
    	},
    	msg_loading_panel: {
    		pt_BR: "Aguarde, preenchendo painel ...",
    		en_US: "Just a few moments, loading panel ...",
    		es: "Espera, llenando el panel ..."
    	},

    	msg_query_server: {
    		pt_BR: "Aguarde, consultando servidor ...",
    		en_US: "Just a few moments, querying server ...",
    		es: "Espera, consultando el servidor ..."
    	},
    	msg_overdue: {
    		pt_BR: "Tarefa Atrasada!",
    		en_US: "Task Overdue!",
    		es: "¡Tarea atrasada!"
    	},
		
		
    	request_error: {
    		pt_BR: "Erro ao obter informações do servidor.",
    		en_US: "Error getting server information.",
    		es: "Error al obtener información del servidor."
    	},
    	unknown_error: {
    		pt_BR: "Ocorreu um erro desconhecido do script do painel.",
    		en_US: "An unknown panel script error has occurred.",
    		es: "Se ha producido un error de script de panel desconocido."
    	},
    	msg_status: {
    		pt_BR: "Status",
    		en_US: "Status",
    		es: "Estado"
    	},
    	msg_quant_tasks: {
    		pt_BR: "Quantidade de Solicitações",
    		en_US: "Number of Requests",
    		es: "Cantidad de solicitudes"
    	},
		deadlines_today_tooltip: {
			pt_BR: "Tarefas vencendo hoje",
			en_US: "Tasks due today",
			es: "Tareas venciendo hoy"
		},
		deadlines_half_tooltip: {
			pt_BR: "Tarefas vencendo em at\u00E9 15 dias",
			en_US: "Due within 15 days",
			es: "Tareas venciendo en 15 d\u00EDas"
		},
		deadlines_whole_tooltip: {
			pt_BR: "Tarefas vencendo entre 15 e 30 dias",
			en_US: "Due between 15 and 30 days",
			es: "Tareas venciendo entre 15 y 30 d\u00EDas"
		},
		list_footer_info: {
			pt_BR: "Exibindo $page de um total de $total solicitações.",
			en_US: "Displaying $page out of $total total requests.",
			es: "Mostrando $page de $total solicitudes totales."
		},
		list_footer_info_zero: {
			pt_BR: "Não há solicitações a para serem exibidas.",
			en_US: "There are no requests to display.",
			es: "No hay solicitudes para mostrar."
		},
		limitInfo: {
			pt_BR: "Limite de 800 solicitações imposto pela versão free.",
			en_US: "800 requests limit due to free version.",
			es: "Limite de 800 solicitudes imposto pela versión free."
		}

    },
    
	// verificação inicial para start do compomente
    startup: function () {
    	if (WCMAPI.version >= "1.6.5") {
    		this.isEditMode ? this.edit() : this.view();
    	} else {

			//TODO: traduzir mensagens
			FLUIGC.message.alert({
				title: 'Esta versão fluig é incompatível com o Task Analyzer.',
				message: 'Este componente necessita do fluig <strong>1.6.5-181218</strong> para funcionar corretamente. Se ainda há dúvidas, acesse a documentação do componente para mais informações: <a href="http://upflow.me/produtos/task-analyzer/docs/inicio/" target="_blank" id="docinfo" class="text-info"><i class="fas fa-question-circle"></i> Documentação</a>',
				label: 'Ok'
			}, function(el, ev) {				 
				$('#app_placeholder, #app_conteudo').remove();
				//window.location.replace(WCMAPI.serverURL);
			});
			
    	}
    },
	
	// inicializa o compomente no modo view
    view: function() {
		
        if (this.preferences['processos'] != "[]" && this.preferences && this.preferences['processos'] != []) {// Check data
			
			//TODO: essa função é basicamente a mesma de "refreshPanel()"
			// melhorar o código para chamar apenas uma vez esse trecho de código
			// cuidado apenas com o placeholder
			
            let self = this;
            
            this.procID = this.preferences['processos'].replace("[","").replace("]","").split(','); // ids dos processos
            this.procDesc = this.preferences['nomeProcessos'].replace("[","").replace("]","").split(','); // descrições dos processos
            
            this.Process = self.procID[0];
            
            this.procID = window.license ? this.procID : [this.procID[0]];
            this.procDesc = window.license ? this.procDesc : [this.procDesc[0]];
            
            // realiza a consulta das tarefas do processo
            this.ufapi.getResume(this.Process, (err, data) => {
                
                if (err) {
                    uFeedback.err(self.i18n.request_error[WCMAPI.locale], err);
                    return false;
                }
                
                self.cleanData = data.items;
                
                // inicializa os elementos do painel
                self.elements.select.init(self.procID, self.procDesc, self.Process, self.preferences);
                self.elements.list.init(self.Process, data.total);
                self.elements.total.init(data.total);
                self.elements.delayed.init(data.resumeProcessTaskStatusSLAVO.expired);
                self.elements.deadlines.init(self.Process);
                self.elements.pie.init(self.Process);
				
				// retira placeholder
				$('#app_placeholder').fadeOut(function(item) { $('#app_conteudo').fadeIn(); $(this).remove(); });
                
            });
            
            $("#refresh").off('click').on('click', function () {
            	if ($('.select2-hidden-accessible').val() != '') {
            		self.refreshPanel($('.select2-hidden-accessible').val(), self.preferences);
            	}
            })
            
        } else {
            
            if(!WCMAPI.isAdmin()) { // usuário logado não é admin

				FLUIGC.message.alert({
					title: 'Task Analyzer não parametrizado',
					message: 'Antes de iniciar a utilização do Task Analyzer, o administrador do fluig deve acessar a página de configuração e realizar as primeiras parametrizações. Acesse a documentação para mais informações: <a href="http://upflow.me/produtos/task-analyzer/docs/admin/#tutorial" target="_blank" id="docinfo" class="text-info"><i class="fas fa-question-circle"></i> Configurando a Widget</a>',
					label: 'Ok'
				}, function(el, ev) {				 
					$('#app_placeholder, #app_conteudo').remove();
					//window.location.replace(WCMAPI.serverURL);
				});
                
            } else { // usuário logado é admin
                $('#app_conteudo').empty();
				
				//TODO: transferir html para o arquivo ftl
                $('#app_placeholder').fadeOut(function(item) { $('#app_conteudo').fadeIn(); $(this).remove(); });
                
	                if (WCMAPI.locale == "pt_BR"){
		                $('#app_conteudo')
			                .append('<div class="row"><div class="col-12 col-md-12"><h3>Task Analyzer: Primeira configuração</h3><hr></div></div>') // header
			                .append('<div class="row"><div class="col-12 col-md-12"><p>Bem Vindo ao Painel Analítico da upFlow!</p><p> Percebemos que você é um administrador do sistema, então guiaremos você nessa primeira etapa de configuração para que você e seus usuários consigam extrair o máximo da nossa widget</p></div></div>')
			                .append('<div class="col-6 col-md-3" style="width:33.33%; margin-left:10%"><div class="card"><h4>1- Edite sua página</h4><p>Procure por um botão escrito "Editar Página" para começar a configuração do seu painel.</p></div></div>')
			                .append('<div class="col-6 col-md-3" style="width:33.33%; margin-top:-7.55%; margin-left:35%"><div class="card"><h4>2- Selecione os processos a serem mostrados</h4><p>Procure pelos processos e os selecione(os selecionados se encontram na coluna da direita)</p></div></div>')
			                .append('<div class="col-6 col-md-3" style="width:33.33%; margin-top:-7.55%; margin-left:60%"><div class="card"><h4>3- Salve e Publique!</h4><p>Clique no botão de salvar e publique sua página. Pronto! Sua widget está configurada e pronta para uso!</p></div></div>')
	                } else if (WCMAPI.locale == "en_US"){
		                $('#app_conteudo')
			                .append('<div class="row"><div class="col-12 col-md-12"><h3>Task Analyzer: Setup</h3><hr></div></div>') // header
			                .append(`<div class="row"><div class="col-12 col-md-12"><p>Welcome to the upFlow's Task Analyzer!</p><p> Looks like you're a system admin, so we will guide you through this setup. After that you and your users will enjoy our widget and it's functions.</p></div></div>`)
			                .append('<div class="col-6 col-md-3" style="width:33.33%; margin-left:10%"><div class="card"><h4>1- Edit your page</h4><p>Try to find a button named "Edit Page" to enter the edit mode and start the setup.</p></div></div>')
			                .append('<div class="col-6 col-md-3" style="width:33.33%; margin-top:-7.55%; margin-left:35%"><div class="card"><h4>2- Select the processes that you wish to show</h4><p>Find the Processes and select them(The selected ones will be shown in the right column)</p></div></div>')
			                .append('<div class="col-6 col-md-3" style="width:33.33%; margin-top:-7.55%; margin-left:60%"><div class="card"><h4>3- Save and Publish!</h4><p>Click on the save button and publish your page. Congratulations! Your widget is now set up and ready to use!</p></div></div>')
	            	}else{
	            		$('#app_conteudo')
		                .append('<div class="row"><div class="col-12 col-md-12"><h3>Task Analyzer: Primera configuración</h3><hr></div></div>') // header
		                .append('<div class="row"><div class="col-12 col-md-12"><p>¡Bienvenido al analizador de tareas de upFlow!</p><p> Parece que eres un administrador del sistema, por lo que te guiaremos a través de esta configuración. Después de eso, usted y sus usuarios disfrutarán de nuestra widget y sus funciones.</p></div></div>')
		                .append('<div class="col-6 col-md-3" style="width:33.33%; margin-left:10%"><div class="card"><h4>1- Edita tu página</h4><p>Intente encontrar un botón llamado "Editar página" para ingresar al modo de edición e iniciar la configuración.</p></div></div>')
		                .append('<div class="col-6 col-md-3" style="width:33.33%; margin-top:-7.55%; margin-left:35%"><div class="card"><h4>2- Seleccione los procesos que desea mostrar.</h4><p>Encuentre los procesos y selecciónelos (los seleccionados se mostrarán en la columna de la derecha)</p></div></div>')
		                .append('<div class="col-6 col-md-3" style="width:33.33%; margin-top:-7.55%; margin-left:60%"><div class="card"><h4>3- ¡Guarda y publica!</h4><p>Haga clic en el botón Guardar y publique su página. ¡Hecho! ¡Su widget ahora está configurada y lista para usar!</p></div></div>')
	            	}
            	}
        }
    },
	
	// inicializa o compomente no modo edit
    edit: function() {
    	
        var self = this;
            	
        this.ufapi.getProcess(true, (err, data) => {

        	msgLoading.setMessage(UFP001.i18n.msg_query_server[WCMAPI.locale]);

            if (err) {
                msgLoading.hide();
                uFeedback.err(self.i18n.msg_error[WCMAPI.locale], err);
                return false;
            }
            
			
			//TODO: melhorar esse código aqui pra baixo
			// está criando variáveis desnecessária
			// e fazendo referências que não fazem sentido (self.AllProcess, self2, etc)

            self.AllProcess = data.items;


            this.el = "#Select .multiple-select"


            this.data = [];
            this.dataName = [];
            var self2 = this;
            
			//TODO: não precisa validar licença no modo edit
            if (!window.license){
				$('select').removeAttr('multiple');
				$('#msg-free-info').append( $('#tpl-freeinfo').html() );
            }

            // Retorna os processos
            self.AllProcess.map(function (elem, index) {
	            self2.data.push(elem.processId);
	            self2.dataName.push(elem.processDescription);
            });

            this.procID = this.preferences['processos'].replace("[", "").replace("]", "").split(','); // ids dos processos
            this.procDesc = this.preferences['nomeProcessos'].replace("[", "").replace("]", "").split(','); // descrições dos processos

            for (i = 1; i < this.procID.length; i++) {
                this.procID[i] = this.procID[i].replace(" ", "");
                this.procDesc[i] = this.procDesc[i].replace(" ", "");
            }

            this.data = this.data.filter((el) => !this.procID.includes(el));
            this.dataName = this.dataName.filter((el) => !this.procDesc.includes(el));

            for (i = 0; i < this.data.length; i++) {
                $(`${this.el} .input-select`).append(' <option name="select_option___' + i + '" value="' + this.data[i] + '">' + this.dataName[i] + ' ('+this.data[i]+')</option>');
            }

            var ClearString, HolderString, SelectedString, nonSelectedString, InfoString, FilteredString, EmptyString;

            switch (WCMAPI.locale) {

                case ("pt_BR"):
                    ClearString = "Mostrar Todos"
                    HolderString = "Pesquisar"
                    SelectedString = "Processos Selecionados"
                    nonSelectedString = "Processos Ativos"
                    InfoString = "Mostrando todos os {0}"
                    FilteredString = "<span class='label label-warning'>Filtrando</span> {0} de {1}"
                    EmptyString = "Lista Vazia"
                    break;

                case ("es"):
                    ClearString = "Mostrar Todo"
                    HolderString = "Buscar"
                    SelectedString = "Procesos Seleccionados"
                    nonSelectedString = "Procesos Activos"
                    InfoString = "Mostrando los {0}"
                    FilteredString = "<span class='label label-warning'>Filtrado</span> {0} de {1}"
                    EmptyString = "Lista Vacía"
                    break;

                case ("en_US"):
                    ClearString = "Show All"
                    HolderString = "Search"
                    SelectedString = "Selected Processes"
                    nonSelectedString = "Active Processes"
                    InfoString = "Showing All {0}"
                    FilteredString = "<span class='label label-warning'>Filtered</span> {0} from {1}"
                    EmptyString = "Empty List"
                    break;

				//TODO: criar uma tradução padrão caso o script não reconheça o WCMAPI.locale informado 

            }

			//TODO: tem formas melhores que iniciarlizar o pluing não sendo criando os <option> manualmente como acima
            $(`${this.el} .input-select`).bootstrapDualListbox({
                filterTextClear: ClearString,
                filterPlaceHolder: HolderString,
                selectedListLabel: SelectedString,
                nonSelectedListLabel: nonSelectedString,
                infoText: InfoString,
                infoTextFiltered: FilteredString,
                infoTextEmpty: EmptyString,
            });

            for (i = 0; i < this.procID.length; i++) {
                if (this.procID != ''){
                id= window.license ? i :this.procID.length- (i+1)
                $(`${this.el} .input-select`).append(' <option name="select_option___' + (this.data.length + id) + '" value="' + this.procID[id] + '" selected>' + this.procDesc[id] +  ' ('+this.procID[id]+')</option>');
                }

            }
            $(`${this.el} .input-select`).bootstrapDualListbox('refresh', true);


            $('.bootstrap-duallistbox-container.moveonselect .moveall, .bootstrap-duallistbox-container.moveonselect .removeall ').css('width', '93.2%');


        });
    },
    
	// atualiza todos os elementos do painel com uma nova consulta ao servidor
    refreshPanel: function(IDProcF, preferences) {
		
		//TODO: melhorar esse código aqui pra baixo
		// está criando variáveis desnecessária
		// e fazendo referências que não fazem sentido (this.procID, self.cleanData = data.items;, etc)
    	
    	let self = this;
		
		msgLoading.show();
		msgLoading.setMessage(UFP001.i18n.msg_query_server[WCMAPI.locale]);
			
		this.procID = preferences['processos'].replace("[","").replace("]","").split(','); // ids dos processos
		this.procDesc = preferences['nomeProcessos'].replace("[","").replace("]","").split(','); // descrições dos processos
		this.preferences = preferences;
		
		this.procID = window.license ? this.procID : [this.procID[0]];
        this.procDesc = window.license ? this.procDesc : [this.procDesc[0]];
			
		// realiza a consulta das tarefas do processo
		this.ufapi.getResume(IDProcF, (err, data) => {

			msgLoading.setMessage(self.i18n.msg_loading_panel[WCMAPI.locale]);

			if (err) {
				msgLoading.hide();
				uFeedback.err(self.i18n.msg_error[WCMAPI.locale], err);
				return false;
			}

			self.cleanData = data.items;

			// inicializa os elementos do painel
			self.elements.select.init(self.procID, self.procDesc, IDProcF, self.preferences);
			self.elements.list.init(IDProcF,data.total); //TODO: mudar para refresh se já estiver inicializado
			self.elements.total.init(data.total);
			self.elements.delayed.init(data.resumeProcessTaskStatusSLAVO.expired);
			self.elements.deadlines.init(IDProcF);
			self.elements.pie.init(IDProcF);

			msgLoading.hide();

		});
		
	},
	
	// indicadores do painel
	elements: {
		
		// controle de licença
		license: {
			
			el: '#app_conteudo',
			
			template: '#tpl-license',
			
			data: null, // armazena se há licença para o componente
			
			init: function() {
				
				if (!this.data) this.render();
				
			},
			
			render: function() {
				
				$(this.el).addClass("demo-version").append( $(this.template).html() );
				$(`${this.el} .header-tools li.demo-info`).show();
				
			},
			
		},
		
		// seleção de processo
		select:{
			
			el:'#app_conteudo .selec-process',
			
			template: null,
			
			data: null,
			
			loading: function(show) {
				show ? FLUIGC.loading(this.el).show() : FLUIGC.loading(this.el).hide() ;
			},
			
			init: function(data, dataName , FirstProcess, preferences) {
				//TODO: trazer o tratamento de dados para essa função (init())
				// e deixar o render apenas para plotar os dados na tela
				this.render(data, dataName, FirstProcess, preferences);
			},
			
			// formato de exibição do processo selecionado
			formatSelect: function (state) {
				if (!state.id) return state.text;
				let template = `<h3>${state.text} <i><small>(${state.id})</small></i></h3>`;
				return $(template);
			},
			
			// formato de exibição da lista de processos
			formatResult: function (state) {
				if (!state.id) return state.text;
				let template = `<span>${state.text} <i><small>(${state.id})</small></i></span>`;
				return $(template);
			},
			
			render: function (data, dataName, FirstProcess, preferences) {

				$(`${this.el} select`).empty();
				
				FirstProcess = FirstProcess.replace(" ", "")
				
				let dataFormat = [];
				let dataNameFormat = [];

				for (i = 0; i < data.length; i++) {
					data[i] = data[i].replace(" ", "");
					
					if (data[i] != FirstProcess) {
						dataFormat.push(data[i])
						dataNameFormat.push(dataName[i])
					} else {
						dataFormat.unshift(data[i])
						dataNameFormat.unshift(dataName[i])
					}
				}
				
				if (dataFormat.length == 1) {
					$("select").prop('disabled', true).parent().addClass('noselect');
				} else {
					$("select").prop('disabled', false).parent().removeClass('noselect');
				}
				
				for (i = 0; i < dataFormat.length; i++) {
					$(`${this.el} select`).append('<option value="' + dataFormat[i] + '">' + dataNameFormat[i] + '</option>');
				}
				
				let select = $(`${this.el} select`).select2({
					width: 'resolve',
					theme: "uF",
					language: WCMAPI.locale == "pt_BR" ? "pt-BR" : WCMAPI.locale,
					templateSelection: this.formatSelect,
					templateResult: this.formatResult,
				});
				
				select.off('select2:select').on('select2:select', function (e) {
					let dataFormat = e.params.data;
					UFP001.refreshPanel(dataFormat.id, preferences);
				});
				
			},
			
		},
		
		// total de tarefas
		total: {
			
			el: '#app_conteudo .card.total',
			
			template: null,
			
			data: null,
			
			loading: function(show) {
				show ? FLUIGC.loading(this.el).show() : FLUIGC.loading(this.el).hide() ;
			},
			
			init: function(Total) {
				
				if (!window.license && Total > 800){
					this.data = "800" + `<a style="cursor: help" id="limit" data-toggle="tooltip" data-placement="right" title="${UFP001.i18n[`limitInfo`][WCMAPI.locale]}">+</a>`;
				} else {
					this.data = Total;
				}
				
				this.render();
				
			},
			
			render: function() {
				
				$(`${this.el} .value`).html( this.data );
				
			},
			
		},
		
		// atrasadas
		delayed: {
			
			el: '#app_conteudo .card.delayed',
			
			template: null,
			
			data: null,
			
			loading: function(show) {
				show ? FLUIGC.loading(this.el).show() : FLUIGC.loading(this.el).hide() ;
			},
			
			init: function(Total) {
							
				if (!window.license && Total > 800){
					this.data = "800" + `<a style="cursor: help" id="limit" data-toggle="tooltip" data-placement="right" title="${UFP001.i18n[`limitInfo`][WCMAPI.locale]}">+</a>`;
				} else {
					this.data = Total;
				}
				
				this.render();
				
			},
			
			render: function() {
				
				$(`${this.el} .value`).html( this.data );
				
			},
			
		},
		
		// próximos vencimentos
		deadlines: {
			
			el: '#app_conteudo .card.deadlines',
			
			template: null,
			
			data: null,
			
			loading: function(show) {
				show ? FLUIGC.loading(this.el).show() : FLUIGC.loading(this.el).hide() ;
			},
			
			clear: function() {
				$(`${this.el} .progress`).empty();
			},
			
			data: {
				today: 0,
				half: 0,
				whole: 0,
				total:0,
			},
			
			init: function (processId) {
				
				this.loading(true); $(`${this.el}`).removeClass('has-error'); this.clear();
				
				let self = this;
				
				UFP001.ufapi.getDeadlines30(processId, (err, data) => {
					
					// tratamento de erro
					if (err) {
						self.loading(false); $(`${self.el}`).addClass('has-error');
						uFeedback.err(UFP001.i18n.request_error[WCMAPI.locale], err);
						return false;
					}
					
					try {
					
						self.data = { today: 0, half: 0, whole: 0, total:0 }; // clean data
						data.items.forEach(function (e, i) {
							if (e.deadlineDate && e.deadlineDate != 0 && e.deadlineDate != undefined) {
								let deadline = moment(e.deadlineDate);
								let dateToday = moment();
								let date15 = moment().add(16, 'days');
								let date30 = moment().add(31, 'days');

								if (deadline.isSame(dateToday, 'day')) self.data.today++;
								if (moment(deadline).isBefore(date15) && moment(deadline).isAfter(dateToday)) self.data.half++;
								if (moment(deadline).isBefore(date30) && moment(deadline).isAfter(date15)) self.data.whole++;
							}
						})
						self.data.total = self.data.today + self.data.half + self.data.whole;

						self.render();

						self.loading(false);
						
					} catch(e) {
						self.loading(false); $(`${self.el}`).addClass('has-error');
						uFeedback.err(UFP001.i18n.unknown_error[WCMAPI.locale], e);
						return false;
					}
					
				});

			},
			
			render: function () {
				
				let BarName = ["today", "half", "whole"];
				let Bar = [(this.data.today / this.data.total) * 100, (this.data.half / this.data.total) * 100, (this.data.whole / this.data.total) * 100] // today, half ,whole
				let Quant = [this.data.today, this.data.half, this.data.whole]
				
				for (let i = 0; i < Bar.length; i++) {
					if (Bar[i] > 0) $(`${this.el} .progress`).append(`<div style="width: ${Bar[i]}%;" data-toggle="tooltip" title="${UFP001.i18n[`deadlines_${BarName[i]}_tooltip`][WCMAPI.locale]}: ${Quant[i]} (${Math.round(Bar[i])}%)" class="progress-bar bar-${BarName[i]}"></div>`);
				}
				
				$('[data-toggle="tooltip"]').tooltip();
			},
			
		},
		
		// lista de pendências
		list: {
			
			el: '#app_conteudo .card.list-card',
			
			template: '#tpl-list',
			
			data: {
				processId: '',
				numberPages: 0,
				total: 0,
				lst: [],
			},
			
			loading: function(show) {
				show ? FLUIGC.loading(this.el).show() : FLUIGC.loading(this.el).hide() ;
			},
			
			clear: function() {
				$(`${this.el} table tbody, ${this.el} .list-footer-info small, ${this.el} .list-footer-pages nav`).empty();
			},
			
			init: function(processId, total) {
				
				this.data.processId = processId;
				this.data.total = total;
				this.data.numberPages = Math.ceil(parseInt(total)/25);
				
				this.refresh(1); // inicia na primeira página
				
			},
			
			refresh: function(page) {
				
				// proteção pois, se 'page' for 0, a API retorna todas as solicitações
				if (page == 0) page = 1;
				
				this.loading(true); $(`${this.el}`).removeClass('has-error'); this.clear();
				
				let self = this;
				
				UFP001.ufapi.getTasks(this.data.processId, page, (err, data) => {
					
					// tratamento de erro
					if (err) {
						self.loading(false); $(`${self.el}`).addClass('has-error');
						uFeedback.err(UFP001.i18n.request_error[WCMAPI.locale], err);
						return false;
					}

					self.data.lst = data.items;

					// adiciona novas funções para mustache
					self.data.deadLineDateFormat = function () {
						if (!this.deadlineDate) return '';
						return moment(this.deadlineDate).format('DD/MM/YYYY HH:mm');
					}
					self.data.deadLineDateFormatXS = function () {
						if (!this.deadlineDate) return '';
						return moment(this.deadlineDate).format('DD/MMM');
					}
					self.data.deadLineDateInfo = function () {
						if (!this.deadlineDate) return '';
						return moment(this.deadlineDate).fromNow();
					}
					self.data.assignSince = function () {
						if (!this.assignStartDate) return '';
						return moment(this.assignStartDate).fromNow();
					}
					self.data.deadLineProgress = function () {

						let tplPgrs = $('#tpl-progress').html(); // template apenas da barra de progresso

						if (this.slaStatus == "EXPIRED") {
							let rendered = Mustache.render(tplPgrs, {
								perc: '100',
								progressClass: 'danger',
								percTitle: `${UFP001.i18n.msg_overdue[WCMAPI.locale]}`,
							});
							return rendered;
						}

						if (!this.deadlineSpecification || this.deadlineSpecification.deadlineTime == 0) return '';
						let perc = (this.deadlineSpecification.spentTime / this.deadlineSpecification.deadlineTime) * 100;
						var progressClass = '';
						switch (true) {
							case (perc >= 70):
								progressClass = 'warning';
								break;
							case (perc >= 90):
								progressClass = 'caution';
								break;
						}
						let percform = Math.round(perc)
						//TODO: renderizar o template igual foi feito nas atividades atrasadas
						return `<div class="progress"><div style="width: ${perc}%;" class="progress-bar ${progressClass}" data-toggle="tooltip" title="${percform}%"></div></div>`;
					}

					self.render(page);

				});
				
			},
		
			render: function(page) {
				
				// renderiza tabela
				let tpl = $(`${this.template}`).html();
				let rendered = Mustache.render(tpl, this.data);
				$(`${this.el} table tbody`).html(rendered);
				
				// renderiza informativo
				if (this.data.lst.length) {
					let info = UFP001.i18n.list_footer_info[WCMAPI.locale];
					info = info.replace('$page',  this.data.lst.length);
					
					if (!window.license && this.data.total > 800){
						info = info.replace('$total',  "800+ (limite da versão free)");
					} else { // batata
						info = info.replace('$total',  this.data.total);
					}
					
					$(`${this.el} .list-footer-info small`).html(info);
				} else {
					$(`${this.el} .list-footer-info small`).html(UFP001.i18n.list_footer_info_zero[WCMAPI.locale]);
				}
				
				// renderiza paginador
				if (this.data.lst.length) {
					let arrPages = [];
					
					var min,max;
					
					if (!window.license && this.data.numberPages > 32){ // batata
						this.data.numberPages = 32;
					}
					
					if (page+2>this.data.numberPages){
						max= this.data.numberPages
					} else {
						max= page+2
					}
					
					if (page-2<1){
						min=1
					} else {
						min= page-2
					}
					
					if (page == 1 && page +4 < this.data.numberPages){
						max= page+4
					}
					if (page == 2 && page +3 < this.data.numberPages){
						max= page+3
					}
					
					if (page == this.data.numberPages-1 && page-3 > 1){
						min= page-3
					}
					if (page == this.data.numberPages && page-4 > 1){
						min= page-4
					}
					
					for (let i = min; i <= max; i++) {
						arrPages.push({ num: i, class: (i == page)?'active':'' })
					}
					let dataPages = {
						lst: arrPages,
						classPrevious: (page == 1)?'disabled':'',
						classNext: (page == this.data.numberPages)?'disabled':'',
						classFirst: (min == 1)?'hidden':'',
						classLast: (max == this.data.numberPages)?'hidden':'',
						last: this.data.numberPages,
						minClass:(min == 1)?'hidden':'',
						maxClass:(max == this.data.numberPages)?'hidden':'',
						min: min,
						max: max
					};
					let tplPage = $('#tpl-list-page').html();
					let renderedPage = Mustache.render(tplPage, dataPages);
					$(`${this.el} .list-footer-pages nav`).html(renderedPage);
				} else {
					$(`${this.el} .list-footer-pages nav`).html('');
				}
				
				this.events(page);
				
				this.loading(false);
				
			},
			
			events: function(page) {
			
				// ao clicar na linha acessa a solicitação
				$(`${this.el} table > tbody > tr[data-id]`).on('click', (event) => {
					
					let processInstanceId = $(event.currentTarget).data('id');
					let url = `${WCMAPI.tenantURL}/pageworkflowview?app_ecm_workflowview_detailsProcessInstanceID=${processInstanceId}`;
					
					window.open(url, '_blank');
					
				});
				
				// troca de página
				$(`${this.el} .list-footer-pages .page-item:not(active) > a`).on('click', (event) => {
					let data = $(event.currentTarget).data();
					
					if (data.topage) this.refresh(data.topage);
					if (data.page) this.refresh(page + data.page);
					
				});
				
				$(`${this.el} table > tbody > tr[data-id] > td[col="sla"] [data-toggle="tooltip"]`).tooltip({  placement: 'auto' });
				
			},
			
		},
		
		// objeto do gráfico de pizza
		pie: {

			el: '#app_conteudo .card.pie-card',

			chart: null,

			data: null,
			
			loading: function(show) {
				show ? FLUIGC.loading(this.el).show() : FLUIGC.loading(this.el).hide() ;
			},
			
			clear: function() {
				$(`${this.el} div.pie-element`).empty();
			},

			init: function (processId) {

				this.clear(); this.loading(true); $(`${this.el}`).removeClass('has-error');
				
				let self = this;
				
				UFP001.ufapi.getStatus(processId, (err, data) => {
					
					// tratamento de erro
					if (err) {
						self.loading(false); $(`${self.el}`).addClass('has-error');
						uFeedback.err(UFP001.i18n.request_error[WCMAPI.locale], err);
						return false;
					}
					
					try {
					
						self.data = [];
						data.items.forEach(solic => {
							

								// tenta localizar o status na lista
								let index = self.data.findIndex(status => { return status[0] == solic.state.stateName });
								
								if (index >= 0) { // já está na lista
									self.data[index][1]++;
								} else { // não está na lista
									self.data.push([solic.state.stateName, 1]);
								}
						});
						self.data.sort((a, b) => { return b[1] - a[1] }); // ordem decrescente
						self.data.unshift([UFP001.i18n.msg_status[WCMAPI.locale], UFP001.i18n.msg_quant_tasks[WCMAPI.locale]]); // adiciona cabeçalho

						self.render();
					
					} catch(e) {
						self.loading(false); $(`${self.el}`).addClass('has-error');
						uFeedback.err(UFP001.i18n.unknown_error[WCMAPI.locale], e);
						return false;
					}
					
				});
			},
			
			render: function() {
				
				let self = this;
				
				google.charts.load('current', { 'packages': ['corechart'] });
				
				google.charts.setOnLoadCallback(() => {
					
					let data = google.visualization.arrayToDataTable( self.data );
					
					let options = {
						legend: {position: 'bottom', alignment: 'center'},
						pieHole: 0.4,
						backgroundColor: 'transparent',
						height: $(`${self.el} div.pie-element`).width() - 80,
						colors:['#6D5CAE','#48B0F7','#10CFBD','#F8D053','#F55753','#3B4752','#00B8D9','#6554C0','#FF5630','#FFAB00','#36B37E','#0052CC','#172B4D'],
					};
					
					// proteção para não exibir offset se tiver apenas um elemento no gráfico
					if (self.data.length > 2) {
						options.slices = {
							0: { offset: 0.1}
						}
					}
					
					self.chart = new google.visualization.PieChart( $(`${self.el} div.pie-element`)[0] );
					self.chart.draw(data, options);
					
					self.loading(false);
					
				});
				
			},
			
		},	
		
	},
	
	// objeto de chamadas ao backend
	ufapi: {
		
		// consulta todas as tarefas de um processo específico
		getProcess: function (active, callback) {
			
			let options = {
				url: `/process-management/api/v2/processes?active=${active}`,
				contentType: 'application/json',
				dataType: 'json',
				loading: false
			};
			
			FLUIGC.ajax(options, callback);
			
		},
		
		// consulta todas as informações necessárias de um processo específico
		getTasks: function (processId, page, callback) {
			let options = {
				url: `/process-management/api/v2/processes/${processId}/requests/tasks?status=NOT_COMPLETED&expand=deadlineSpecification&assignee=${WCMAPI.userLogin}&pageSize=25&page=${page}&order=deadline`,
				contentType: 'application/json',
				dataType: 'json',
				loading: false
			};
			FLUIGC.ajax(options, callback);
		},
		getDeadlines30: function (processId, callback) {
			let date = moment().add(30, 'days').format("YYYY-MM-DDTHH:mm:ssZ")
			let options = {
				url: `/process-management/api/v2/processes/${processId}/requests/tasks?status=NOT_COMPLETED&fields=deadlineDate&assignee=${WCMAPI.userLogin}&finalDeadlineDate=${date}`,
				contentType: 'application/json',
				dataType: 'json',
				loading: false
			};
			FLUIGC.ajax(options, callback);
		},
		getStatus: function (processId, callback) {
			let limit = window.license ? 9999 : 800
			let options = {
				url: `/process-management/api/v2/processes/${processId}/requests/tasks?status=NOT_COMPLETED&fields=state&assignee=${WCMAPI.userLogin}&pageSize=${limit}`,
				contentType: 'application/json',
				dataType: 'json',
				loading: false
			};
			FLUIGC.ajax(options, callback);
		},
		getResume: function (processId, callback) {
			let options = {
				url: `/process-management/api/v2/processes/${processId}/requests/tasks/resume?status=NOT_COMPLETED&assignee=${WCMAPI.userLogin}`,
				contentType: 'application/json',
				dataType: 'json',
				loading: false
			};
			FLUIGC.ajax(options, callback);
		},
		
		// consulta a licença do componente no License Server
		serviceCheckSlotId: function (callback) {
			let options = {
				url: `/license/api/v1/slots/${UFP001slotId}`,
				contentType: 'application/json',
				dataType: 'json',
				loading: false
			};
			FLUIGC.ajax(options, callback);
		},
		
	}
    
});

/**
 * Objeto padrão de loading
 */
window.msgLoading = FLUIGC.loading('body', {
  textMessage: UFP001.i18n.msg_query_server[WCMAPI.locale],
  overlayCSS: {
    backgroundColor: '#000',
    opacity: 0.6,
    cursor: 'wait'
  },
  baseZ: 1000,
  fadeIn: 200,
  fadeOut: 400,
  timeout: 0,
})

/**
 * Objeto de feedback ao usuário
 */
window.uFeedback = {
	
	err: function(msg, details) {
		
		// TODO: adicionar modal para abrir chamado no Service Desk da uF
		
	   	FLUIGC.message.error({
    	    title: 'Ops, ocorreu um erro',
    	    message: msg,
    	    details: details,
    	    label: 'Ok'
    	}, function(el, ev) {
    	    
    	});
		
	},
	
}