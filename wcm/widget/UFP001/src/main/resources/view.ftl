<#assign reposi='UFP001'>
<#assign versao='1.0.0'>
<#assign parametros="{processos: '${processos!}', nomeProcessos: '${nomeProcessos!}'}">

<script type="text/javascript">const ${reposi}slotId = 3159</script>

<#if pageRender.isEditMode()=false>
	<link rel="stylesheet" href="/${reposi}/resources/css/external/bootstrap-reboot.min.css">
	<link rel="stylesheet" href="/${reposi}/resources/css/external/bootstrap-grid.min.css">
</#if>

<div id="${reposi}_${instanceId}" class="uF ${reposi} super-widget wcm-widget-class container-fluid" data-params="${reposi}.instance({reposi:'${reposi}', versao:'${versao}', widgetId: ${instanceId}, preferences: ${parametros}})">

	<div id="app_placeholder">
		<div class="row header">
			<div class="col-5">
				<div class="text" style="margin-bottom: 10px;"></div>
				<div class="text small" style="width: 50%;"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-6 col-md-3">
				<div class="card total">
					<div class="row align-items-center">
						<div class="col">
							<h5 class="text"></h5>
							<div class="text value"></div>
						</div>
						<div class="col-auto d-none d-md-block">
							<div class="text icon"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-6 col-md-3">
				<div class="card delayed">
					<div class="row align-items-center">
						<div class="col">
							<h5 class="text"></h5>
							<div class="text value"></div>
						</div>
						<div class="col-auto d-none d-md-block">
							<div class="text icon"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md">
				<div class="card deadlines">
					<div class="row align-items-center">
						<div class="col">
							<h5 class="text value"></h5>
							<div class="text small"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-5 order-md-last">
				<div class="card pie-card">
					<div class="row">
						<div class="col">
							<h5 class="text value"></h5>
						</div>
					</div>
					<div class="row panel-content"></div>
				</div>
			</div>
			<div class="col-12 col-md">
				<div class="card">
					<div class="row">
						<div class="col">
							<h5 class="text value"></h5>
						</div>
					</div>
					<div class="row panel-content"></div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- INÍCIO: conteúdo principal da página -->
	<div id="app_conteudo" style="display: none;">

		<div class="row align-items-center header">
			<div class="col">
				<div class="selec-process">
					<select style="width: 100%"></select>
				</div>
			</div>
			<div class="col-auto d-none d-sm-block">
				<ul class="header-tools">
					<li class="demo-info" style="display: none;">
						<span class="badge" data-toggle="tooltip" title="${i18n.getTranslation('demo.info')}" data-placement="bottom">Free</span>
					</li>
					<li class="doc-link">
						<a href="http://upflow.me/produtos/task-analyzer/docs/inicio/" target="_blank" id="docinfo" data-toggle="tooltip" data-placement="bottom" title="${i18n.getTranslation('doc.link')}"><i class="fas fa-question-circle"></i></a>
					</li>
					<li class="refresh-all">
						<a href="javascript:;" id="refresh" data-toggle="tooltip" data-placement="bottom" title="${i18n.getTranslation('refresh.all')}"><i class="fas fa-sync-alt"></i></a>
					</li>
				</ul>
			</div>
		</div>

		<div class="row">
			<div class="col-6 col-md-3">
				<div class="card total">

					<div class="row align-items-center">
						<div class="col">
							<h5 class="panel-title">${i18n.getTranslation('card.total')} <span class="info" data-toggle="tooltip" data-placement="auto" title="${i18n.getTranslation('card.total.info')}"><i class="fas fa-info-circle"></i></span></h5>
							<span class="value">0</span>
						</div>
						<div class="col-auto d-none d-md-block">
							<i class="fas fa-arrow-alt-circle-up fa-2x"></i>
						</div>
					</div>

				</div>
			</div>
			<div class="col-6 col-md-3">
				<div class="card delayed">

					<div class="row align-items-center">
						<div class="col">
							<h5 class="panel-title">${i18n.getTranslation('card.late')} <span class="info" data-toggle="tooltip" data-placement="auto" title="${i18n.getTranslation('card.late.info')}"><i class="fas fa-info-circle"></i></span></h5>
							<span class="value">0</span>
						</div>
						<div class="col-auto d-none d-md-block">
							<i class="fas fa-exclamation-circle fa-2x"></i>
						</div>
					</div>

				</div>
			</div>
			<div class="col-12 col-md">
				<div class="card deadlines">

					<div class="row align-items-center">
						<div class="col">
							<h5 class="panel-title">${i18n.getTranslation('card.deadlines')} <span class="info" data-toggle="tooltip" data-placement="auto" title="${i18n.getTranslation('card.deadlines.info')}"><i class="fas fa-info-circle"></i></span></h5>
							<div class="progress"></div>
							<div class="row progress-footer">
								<div class="col">
									<i class="fas fa-circle  icon-today"></i> <span>${i18n.getTranslation('card.deadlines.today')}</span> |
									<i class="fas fa-circle icon-half"></i> <span>${i18n.getTranslation('card.deadlines.half')}</span> |
									<i class="fas fa-circle icon-whole"></i> <span>${i18n.getTranslation('card.deadlines.whole')}</span>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-md-5 order-md-last">
				<div class="card pie-card">

					<div class="row panel-header">
						<div class="col title">
							<h5 class="panel-subtitle">${i18n.getTranslation('card.pie.subtitle')}</h5>
							<h5 class="panel-title">${i18n.getTranslation('card.pie')} <span class="info" data-toggle="tooltip" data-placement="auto" title="${i18n.getTranslation('card.pie.info')}"><i class="fas fa-info-circle"></i></span></h5>
						</div>
					</div>
					<div class="row panel-content">
						<div class="col">

							<div class="pie-element"></div>

						</div>
					</div>

				</div>
			</div>
			<div class="col-12 col-md">
				<div class="card list-card">

					<div class="row panel-header">
						<div class="col title">
							<h5 class="panel-subtitle">${i18n.getTranslation('card.list.subtitle')}</h5>
							<h5 class="panel-title">${i18n.getTranslation('card.list')} <span class="info" data-toggle="tooltip" data-placement="auto" title="${i18n.getTranslation('card.list.info')}"><i class="fas fa-info-circle"></i></span></h5>
						</div>

					</div>
					<div class="panel-content">
						<div class="row">
							<div class="col tableHolder">
								<table>
									<thead>
										<tr>
											<th scope="col" width="1%"><span class="d-none d-sm-block">${i18n.getTranslation('table.head.request')}</span><span class="d-block d-sm-none">#</span></th>
											<th scope="col">${i18n.getTranslation('table.head.status')}</th>
											<th scope="col" width="1%">${i18n.getTranslation('table.head.deadline')}</th>
											<th scope="col" width="30%">${i18n.getTranslation('table.head.sla')}</th>
											<th scope="col" width="1%"></th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						<div class="row list-footer align-items-center">
							<div class="col list-footer-info">
								<small class="d-none d-sm-block"></small>
							</div>
							<div class="col-auto list-footer-pages">
								<nav></nav>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
	<!-- FINAL: conteúdo principal da página -->

</div>

<#if pageRender.isEditMode()=false>
	<script type="text/javascript" src="/${reposi}/resources/js/external/all.js"></script>

	<script src="/${reposi}/resources/js/external/popper.min.js"></script>
	<script src="/${reposi}/resources/js/external/bootstrap.min.js"></script>

	<script type="text/javascript" src="/${reposi}/resources/js/external/loader.js"></script>

	<script type="text/javascript" src="/${reposi}/resources/js/external/select2.min.js"></script>
	<script src="/${reposi}/resources/js/external/pt-BR.js"></script>
	<script src="/${reposi}/resources/js/external/es.js"></script>
	<script src="/${reposi}/resources/js/external/en.js"></script>
</#if>

<!--Template para lista de solicitações-->
<script type="text/html" id="tpl-list">
{{#lst}}
	<tr data-id="{{processInstanceId}}">
		<td col="nbr">#<span>{{processInstanceId}}</span></td>
		<td col="stt"><span>{{state.stateName}}</span><small>{{assignSince}}</small></td>
		<td col="dln"><span class="d-none d-sm-block">{{deadLineDateFormat}}</span><span class="d-block d-sm-none">{{deadLineDateFormatXS}}</span><small>{{deadLineDateInfo}}</small></td>
		<td col="sla">{{{deadLineProgress}}}</td>
		<td col="act"><i class="fas fa-angle-right fa-2x" data-toggle="tooltip" data-placement="right" title="${i18n.getTranslation('card.list.access')}"></i></td>
	</tr>
{{/lst}}
{{^lst}}
	<tr class="no-tasks">
		<td align="center" colspan="5"><span><i class="fas fa-exclamation-circle"></i></span> ${i18n.getTranslation('card.list.noTasks')}</td>
	</tr>
{{/lst}}
</script>
	
<!--Template para paginação da lista-->
<script type="text/html" id="tpl-list-page">
	<ul class="pagination pagination-sm">
		<li class="page-item {{classPrevious}}"><a class="page-link" href="javascript:;" data-page="-1"><i class="fas fa-chevron-left"></i></a></li>
		<li class="page-item {{classFirst}}"><a class="page-link" href="javascript:;" data-topage="1">1</a></li>
		<li class="page-item {{minClass}}"><a class="page-link" href="javascript:;" data-topage="{{min}}">...</a></li>
		{{#lst}}
		<li class="page-item {{class}}"><a class="page-link" href="javascript:;" data-topage="{{num}}">{{num}}</a></li>
		{{/lst}}
		<li class="page-item {{maxClass}}"><a class="page-link" href="javascript:;" data-topage="{{max}}">...</a></li>
		<li class="page-item {{classLast}}"><a class="page-link" href="javascript:;" data-topage='{{last}}'>{{last}}</a></li>
		<li class="page-item {{classNext}}"><a class="page-link" href="javascript:;" data-page="1"><i class="fas fa-chevron-right"></i></a></li>
		
	</ul>
</script>

<!--Template de progess bar-->
<script type="text/html" id="tpl-progress">
	<div class="progress">
		<div style="width: {{perc}}%;" class="progress-bar {{progressClass}}" data-toggle="tooltip" title="{{percTitle}}"></div>
	</div>
</script>

<!--Template de call to action para não licença-->
<script type="text/html" id="tpl-license">
	<div class="row">
		<div class="col">
			<div class="card license">
				<div class="row align-items-center">
					<div class="col-12 col-md-auto">
						<img src="/${reposi}/resources/images/UFP001.svg" alt="Task Analyzer Logo" width="80px" />
					</div>
					<div class="col">
						<h4>Task Analyzer <i><small>by upFlow.me</small></i></h4>
						<span>${i18n.getTranslation('card.license.info')}</span>
					</div>
					<div class="col-12 col-md-auto">
						<a href="https://www.fluigstore.com/" target="_blank" class="button">
							<div class="icons">
								<i class="fas fa-shopping-cart icon-default"></i>
								<i class="fa fa-thumbs-up icon-hover"></i>
							</div>
							${i18n.getTranslation('card.license.callaction')}
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</script>

<script type="text/javascript" src="/${reposi}/resources/js/${reposi}.js?v=${versao}" charset="utf-8"></script>