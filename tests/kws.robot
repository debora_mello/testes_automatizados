**Settings**
Documentation    Keywords genericas

**Keywords**
Close Session
    Capture Page Screenshot
    Close All Browsers
    
Open Chrome
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Call Method    ${chrome_options}    add_argument    --disable-dev-shm-usage
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size     1366    768

loginFluig
    [Documentation]  Faz o login no fluig. Recomendado para ser usado como setup, ou 1a kw para um setup.
    [Arguments]  ${url}  ${login}  ${pw}  ${browser}=chrome  ${inputLogin}=id:username  ${inputPw}=id:password  ${btnAcessar}=id:submitLogin  
        Open Chrome
        Maximize Browser Window
        Go To                      ${url}
        Wait Until Element Is Visible                       ${inputLogin}    15s
        Wait Until Element Is Visible                       ${inputPw}       15s
        Input Text                 ${inputLogin}        ${login}
        Input Text                 ${inputPw}           ${pw}
        Click                      ${btnAcessar}

Click
    [Arguments]         ${LOCATOR}
        Wait                ${LOCATOR}
        Click Element       ${LOCATOR}

Wait   
    [Arguments]         ${LOCATOR}      ${TIMEOUT}=40s
        Set Selenium Timeout  ${TIMEOUT}
        Set Suite Variable    ${TIMEOUT}
        Wait Until Page Contains Element        ${LOCATOR}

Aparecer_Desaparecer
    [Arguments]         ${LOCATOR}
        Wait                                        ${LOCATOR}
        Wait Until Page Does Not Contain Element   ${LOCATOR}  

dropdownEnviar
    [Arguments]     ${action}=Enviar  ${alert}=False
        
        ${status}  Run Keyword And Return Status  Run Keyword and Expect Error  The element * should be visible, but it is not.   Element Should Be Visible   xpath://div[contains(@id, 'workflowActions')]/button[contains(text(), 'Enviar')]
        
        ${enviar}           Set Variable If  '${status}'=='True'    xpath://div[contains(@class, 'fixedTopBar')]//button[contains(text(), 'Enviar')]
        ...                                  '${status}'=='False'   xpath://div[contains(@id, 'workflowActions')]/button[contains(text(), 'Enviar')]  
        ${dropdown}         Set Variable If  '${status}'=='True'    xpath://div[contains(@class, 'fixedTopBar')]//button[contains(@data-toggle, 'dropdown')]
        ...                                  '${status}'=='False'   xpath://div[contains(@id, 'workflowActions')]/button[contains(@data-toggle, 'dropdown')]
        ${dropdownClick}    Set Variable If  '${status}'=='True'    xpath://div[contains(@class, 'fixedTopBar')]//a[contains(text(), '${action}')]
        ...                                  '${status}'=='False'   xpath://div[contains(@id, 'workflowActions')]//a[contains(text(), '${action}')]
        ${success}          Set Variable  xpath://div[contains(@id, 'ecm_wkfview')]//div[contains(@class, 'title')]
        
        Element Should Be Visible  ${enviar}
        Run Keyword If  '${action}'=='Enviar' and '${alert}'=='True'    Run Keywords    Click   ${enviar}
        ...  AND                                                                        Handle Alert
        ...  ELSE IF    '${action}'=='Enviar' and '${alert}'!='True'    Click   ${enviar}
        Run Keyword If  '${action}'=='Salvar'     Run Keywords  Click   ${dropdown}
        ...                                       AND           Click   ${dropdownClick}
        Run Keyword If  '${action}'=='Descartar'  Run Keywords  Click   ${dropdown}
        ...                                       AND           Click   ${dropdownClick}
        ...                                       AND           Click   xpath://div[contains(@class, 'modal-footer')]/button[contains(text(), 'Sim')]
        ...                                       AND           Title Should Be     Fluig - Central de Tarefas 
        
fecharAlerta
    [Arguments]  ${modal}=css:#fluig-modal .modal-content  ${btnX}=css:#fluig-modal .close
        Register Keyword To Run On Failure   NONE
        Run Keyword And Ignore Error  Wait  ${modal}       
        Run Keyword And Ignore Error  Click  ${btnX}        

