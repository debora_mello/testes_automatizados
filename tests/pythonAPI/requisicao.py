import requests
from requests_oauthlib import OAuth1

def url(processId, assignee):
    return "http://portal.upflow.me/api/public/2.0/workflows/requests/tasks/resume?processId={}&assignee={}".format(processId, assignee)

auth = OAuth1('321689832198', '321689832198', 'bad1179f-2978-4d67-85c2-ab9499e77a98', 'be88e0e3-46ee-4f1c-88e8-ec976e62c3a66f7df750-4008-4156-a45c-08d5fbc0c1f3')


def getResume(processId, assignee):
    response = requests.get(url(processId, assignee), auth=auth)
    json = response.json()
    if response.status_code != 200:
        return json['code'], json['message']
    else:
        totalPendencias = json['content']['total']
        atrasadas = json['content']['expired']
        return json, totalPendencias, atrasadas

