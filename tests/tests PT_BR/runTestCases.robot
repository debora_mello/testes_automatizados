***Settings***
Documentation    Suite dos testes do T.A v1 em portugues.
Resource        ../base.robot

Suite Setup     Register Keyword To Run On Failure   NONE
Test Teardown   Close Session

***Test Cases***
Validação do placeholder no carregamento inicial do T.A
    [Documentation]  Teste referente a tarefa: https://upflow.atlassian.net/browse/IEC-15?atlOrigin=eyJpIjoiZTg5YzRjOTI5NDdmNDRmM2E1NGE0OWMwNWIwZmRiNTUiLCJwIjoiaiJ9
    [Setup]  Acessar o Fluig
    # fecharAlerta  #não há alerta no fluig PORTAL
    Acessar o T.A
    Run Keyword And Ignore Error    Validar Placeholder inicial

Verificações do elemento Total de Pendências
    [Documentation]  Teste referente a tarefa: https://upflow.atlassian.net/browse/IEC-2?atlOrigin=eyJpIjoiMWJhN2QxNzI4NTA5NDJkYjgyNTAzNzQ3MjU3N2M4NWYiLCJwIjoiaiJ9
    [Setup]  Acessar o TA em "portugues" aguardando o placeholder
    Wait  css:.total 
    ${panelTitle-text}  Get Text  css:.total .panel-title
    Should Be Equal     ${panelTitle-text}  TOTAL DE PENDÊNCIAS
    ${totalPendencias}  Get Text  css:.total .value
    ${api_totalPendencias}  requisicao.Get Resume  FLUIGADHOC  fluigadmin
    ${conferencia}  Execute Javascript  return ${api_totalPendencias[1]} == ${totalPendencias}
    ${tooltip-text}  Execute Javascript  return $('.total span.info').attr('data-original-title')
    Should Be Equal  ${tooltip-text}  Total de tarefas que estão sob sua responsabilidade
    ${escondido}    o tooltip ".total span.info" está "hidden"   
    Mouse Over      css:.total
    ${visivel}      o tooltip ".total span.info" está "visible"
    #Verifica se o icone do quadrante de total de pendencias está visível
    ${display-icon}  Execute Javascript  return $('.d-md-block').css('display') == "block"
    ${isNumber}  Execute Javascript  return !isNaN($('.total .value').text())
    Should Be True  ${conferencia}
    Should Be True  ${escondido}  
    Should Be True  ${visivel}  
    Should Be True  ${display-icon}  
    Should Be True  ${isNumber}

Verificações do elemento Solicitações Atrasadas
    [Documentation]  Teste referente a tarefa: https://upflow.atlassian.net/browse/IEC-2?atlOrigin=eyJpIjoiMWJhN2QxNzI4NTA5NDJkYjgyNTAzNzQ3MjU3N2M4NWYiLCJwIjoiaiJ9
    [Setup]  Acessar o TA em "portugues" aguardando o placeholder
    Wait  css:.delayed 
    ${panelTitle-text}  Get Text  css:.delayed .panel-title
    Should Be Equal     ${panelTitle-text}  SOLICITAÇÕES ATRASADAS
    ${solicAtrasada}    Get Text  css:.delayed .value
    ${api_atrasadas}    requisicao.Get Resume  FLUIGADHOC  fluigadmin
    ${conferencia}      Execute Javascript  return ${api_atrasadas[2]} == ${solicAtrasada}
    ${tooltip-text}     Execute Javascript  return $('.delayed span.info').attr('data-original-title')
    Should Be Equal  ${tooltip-text}  Total de tarefas atrasadas sob sua responsabilidade
    ${escondido}    o tooltip ".delayed span.info" está "hidden"   
    Mouse Over      css:.delayed
    ${visivel}      o tooltip ".delayed span.info" está "visible"
    #Verifica se o icone do quadrante de solicitações atrasadas está visível
    ${display-icon}  Execute Javascript  return $('.d-md-block').css('display') == "block"
    ${isNumber}     Execute Javascript  return !isNaN($('.delayed .value').text())
    Should Be True  ${conferencia}
    Should Be True  ${escondido}  
    Should Be True  ${visivel}  
    Should Be True  ${display-icon}  
    Should Be True  ${isNumber}